package fi.bugbyte.spacehaven;

import com.badlogic.gdx.utils.Array;
import fi.bugbyte.framework.Game;
import fi.bugbyte.framework.library.AssetLibrary;
import fi.bugbyte.spacehaven.gui.GUI;
import fi.bugbyte.spacehaven.stuff.Character;
import fi.bugbyte.spacehaven.stuff.Production;
import fi.bugbyte.spacehaven.stuff.personality.Personality;
import fi.bugbyte.spacehaven.world.Ship;
import fi.bugbyte.spacehaven.world.World;
import fi.bugbyte.spacehaven.world.elements.Storage;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class EditorReflected {
    private boolean shipSet = false;
    private MyStorageTableModel storageData;
    private MyCharacterDataModel characterData;
    private Ship currentShip = null;
    private Array<Character> characters = null;
    private Character currentCharacter = null;
    private Storage.ItemStorage currentStorage = null;
    Array<Storage.ItemStorage> storages = null;

    Array<Storage.StoredItem> items = null;


    private void updateCurrentShip(Ship newShip) {
        currentShip = newShip;
        characters = currentShip.getCharacters();
        storages = currentShip.getStorage();
        if (storages != null && storages.size > 0) {
            Storage.ItemStorage storage = storages.get(0);
            Storage.Inventory inventory = storage.getInventory();
            items = inventory.getStuff();
            characterSelector.updateUI();
            storageSelector.updateUI();
        }

    }

    public EditorReflected() {
        storageTable.addPropertyChangeListener(evt -> {

        });
        initializeBtn.addActionListener(e -> {
            if (!shipSet) {
                try {

                    GUI gui = GUI.instance;
                    World world = gui.getWorld();
                    Array<Ship> allShips = world.getShips();
                    Array<Ship> playerShips = filterPlayerList(allShips);
                    maxCurrentCharButton.addActionListener(e1 -> {
                        if (currentCharacter != null) {
                            Array<Personality.Skill> skills = currentCharacter.getPersonality().getSkills();
                            for (Personality.Skill skill : skills) {
                                skill.level = 3;

                            }
                            characterTable.updateUI();
                        }
                    });
                    maxCurrentStorageButton.addActionListener(e1 -> {
                        if (currentStorage != null) {
                            Array<Storage.StoredItem> stuff = currentStorage.getInventory().getStuff();
                            for (Storage.StoredItem storedItem : stuff) {
                                storedItem.inStorage = 50;
                            }
                            storageTable.updateUI();
                        }
                    });

                    shipSelector.setModel(new ComboBoxModel() {
                                              int selected;

                                              @Override
                                              public int getSize() {
                                                  return playerShips.size;
                                              }

                                              @Override
                                              public Object getElementAt(int index) {
                                                  selected = index;
                                                  Ship selectedShip = playerShips.get(selected);
                                                  return selectedShip.getName();
                                              }

                                              @Override
                                              public void addListDataListener(ListDataListener l) {

                                              }

                                              @Override
                                              public void removeListDataListener(ListDataListener l) {

                                              }

                                              @Override
                                              public void setSelectedItem(Object anItem) {
                                                  currentShip = playerShips.get(selected);
                                                  updateCurrentShip(currentShip);
                                                  shipSet = true;
                                              }

                                              @Override
                                              public Object getSelectedItem() {
                                                  setSelectedItem(getElementAt(selected));
                                                  return getElementAt(selected);
                                              }
                                          }

                    );
                    storageSelector.setModel(new ComboBoxModel() {
                        int selected;

                        @Override
                        public int getSize() {
                            return storages.size;
                        }

                        @Override
                        public Object getElementAt(int index) {
                            return index;
                        }

                        @Override
                        public void addListDataListener(ListDataListener l) {

                        }

                        @Override
                        public void removeListDataListener(ListDataListener l) {

                        }

                        @Override
                        public void setSelectedItem(Object anItem) {
                            selected = (int) anItem;
                            if (storageData != null) {

                                Storage.ItemStorage itemStorage = storages.get(selected);
                                currentStorage = itemStorage;
                                storageData.items = itemStorage.getInventory().getStuff();
                                storageTable.updateUI();
                            }
                        }

                        @Override
                        public Object getSelectedItem() {
                            setSelectedItem(getElementAt(selected));
                            return selected;
                        }
                    });

                    characterSelector.setModel(new ComboBoxModel() {
                        int selected = 0;
                        String selectedName = "unset";

                        @Override
                        public int getSize() {
                            return characters.size;
                        }

                        @Override
                        public Object getElementAt(int index) {
                            selected = index;
                            Character character = characters.get(selected);
                            selectedName = character.getName();

                            return selectedName;
                        }

                        @Override
                        public void addListDataListener(ListDataListener l) {

                        }

                        @Override
                        public void removeListDataListener(ListDataListener l) {

                        }

                        @Override
                        public void setSelectedItem(Object anItem) {
                            if (characterData != null) {
                                Character character = characters.get(selected);
                                characterData.skills = character.getPersonality().getSkills();
                                currentCharacter = character;
                                characterTable.updateUI();
                            }
                        }

                        @Override
                        public Object getSelectedItem() {
                            setSelectedItem(getElementAt(selected));
                            return selectedName;
                        }
                    });

                    storageData = new MyStorageTableModel(new Array<>());

                    characterData = new MyCharacterDataModel(new Array<>());

                    storageTable.setModel(storageData);
                    characterTable.setModel(characterData);


                    DefaultTableCellRenderer alignedRenderer = new DefaultTableCellRenderer();
                    alignedRenderer.setHorizontalAlignment(JLabel.RIGHT);

//                    alignedRenderer.setFont(new Font(alignedRenderer.getFont().getName(), Font.BOLD, 14));

                    storageTable.getColumnModel().getColumn(0).setCellRenderer(alignedRenderer);
                    characterTable.getColumnModel().getColumn(0).setCellRenderer(alignedRenderer);

//                    characterTable.setFont(new Font(alignedRenderer.getFont().getName(), Font.BOLD, 14));

                    storageTable.setRowHeight(18);
                    characterTable.setRowHeight(18);
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }
        });
    }

    private Array<Ship> filterPlayerList(Array<Ship> ships) {
        Array<Ship> playerShips = new Array<>();
        for (Ship s : ships) {
            if (s.isPlayerShip()) {
                playerShips.add(s);
            }
        }
        return playerShips;
    }


    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {

        JFrame frame = new JFrame("Editor");
        frame.setContentPane(new EditorReflected().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        MainClass.main(new String[]{""});


    }

    private JPanel panel1;
    private JTable storageTable;
    private JComboBox storageSelector;
    private JButton initializeBtn;
    private JComboBox characterSelector;
    private JTable characterTable;
    private JComboBox shipSelector;
    private JButton maxCurrentStorageButton;
    private JButton maxCurrentCharButton;

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    private static class MyStorageTableModel extends AbstractTableModel {
        private Array<Storage.StoredItem> items;
        private final String[] columnNames = {"Element Name", "Amount"};
        private final HashMap<Integer, String> idToName = new HashMap<>();
        ElementLibrary oLibrary;
        AssetLibrary oLibrary2;

        public MyStorageTableModel(Array<Storage.StoredItem> t) throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
            items = t;
            oLibrary = SpaceHaven.library;
            oLibrary2 = Game.library;
        }

        @Override
        public int getRowCount() {
            return items.size;
        }

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public String getColumnName(int columnIndex) {
            return columnNames[columnIndex];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if (columnIndex == 0) {
                return String.class;
            } else {
                return Integer.class;
            }
        }


        private String getName(int i) {
            Production.Elementary t = null;
            String s = "error";
            t = oLibrary.getElementary(i);
            if (t != null) {
                s = oLibrary2.getTextById(t.name.id).getText();
            } else {
                s = Integer.toString(i);
            }
            return s;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (columnIndex == 0) {
                int i = items.get(rowIndex).elementaryId;
                String s = idToName.get(i);
                if (s == null) {
                    s = getName(i);
                    idToName.put(i, s);
                }
                return s;
            } else {
                return items.get(rowIndex).inStorage;
            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            try {
                items.get(rowIndex).inStorage = (int) aValue;
                fireTableCellUpdated(rowIndex, columnIndex);
            } catch (Exception ignored) {

            }
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnIndex == 1;
        }
    }

    private static class MyCharacterDataModel extends AbstractTableModel {
        private Array<Personality.Skill> skills;
        private final String[] columnNames = {"Skill", "Level"};

        public MyCharacterDataModel(Array<Personality.Skill> t) {
            skills = t;
        }

        @Override
        public int getRowCount() {
            return skills.size;

        }

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public String getColumnName(int columnIndex) {
            return columnNames[columnIndex];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if (columnIndex == 0) {
                return String.class;
            } else {
                return Integer.class;
            }
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (columnIndex == 0) {
                return skills.get(rowIndex).skill.getName();

            } else {
                return skills.get(rowIndex).level;

            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            try {
                skills.get(rowIndex).level = (Integer) aValue;
                fireTableCellUpdated(rowIndex, columnIndex);
            } catch (Exception ignored) {

            }
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnIndex == 1;
        }
    }
}
